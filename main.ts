import { find } from 'better-sqlite3-proxy'
import { mkdirSync, writeFileSync } from 'fs'
import { join } from 'path'
import { chromium } from 'playwright'
import { proxy } from './proxy'
import { getSiteId } from './store'
import fetch from 'node-fetch'
import { db } from './db'

async function main() {
  let browser = await chromium.launch({ headless: false })
  let page = await browser.newPage()
  let category_id = '2110--29--434--35--5359--28--30--31'
  let domain = 'www.zalora.com.hk'
  let site_id = getSiteId(domain)
  let dirname = join('data', 'images', domain)
  mkdirSync(dirname, { recursive: true })
  for (let pageNum = 1; ; pageNum++) {
    await page.goto(
      `https://www.zalora.com.hk/men/clothing/?page=${pageNum}&category_id=${category_id}`,
    )
    let { links, maxPage } = await page.evaluate(() => {
      return new Promise<{ links: string[]; maxPage: number }>(
        (resolve, reject) => {
          function loop() {
            let links = Array.from(
              document.querySelectorAll<HTMLAnchorElement>(
                '#content ul.b-catalogList__wrapper li a.itm-link',
              ),
            ).map(a => a.href)
            if (links.length === 0) {
              setTimeout(loop, 33)
              return
            }
            let maxPage = 0
            document.querySelectorAll('.page-item').forEach(e => {
              let page = +e.textContent!
              if (page > maxPage) {
                maxPage = page
              }
            })
            resolve({ links, maxPage })
          }
          loop()
        },
      )
    })
    let i = 0
    for (let productUrl of links) {
      i++
      if (find(proxy.product, { url: productUrl })) {
        continue
      }
      await page.goto(productUrl)
      let { title, images } = await page.evaluate(() => {
        let title = document
          .querySelector('.product__title')
          ?.textContent?.trim()
        if (!title) throw new Error('failed to find product title')
        let images = Array.from(
          document.querySelectorAll<HTMLImageElement>(
            'ul.prd-moreImagesList li img',
          ),
        ).map(img => {
          let src = img.src.match(/.*(https:\/\/.*)/)?.[1]
          if (!src) {
            throw new Error('failed to parse image url')
          }
          return src
        })
        if (images.length === 0) throw new Error('failed to find images')
        return { title, images }
      })
      await db.transaction(async () => {
        let product_id = proxy.product.push({ site_id, title, url: productUrl })
        for (let imageUrl of images) {
          let filename = imageUrl.match(/.*\/(.*)/)?.[1]
          if (!filename) {
            throw new Error('failed to parse image filename')
          }
          let file = join(dirname, filename)
          let buffer = await fetch(imageUrl).then(res => {
            if (!res.ok) {
              throw new Error('failed to download image')
            }
            return res.buffer()
          })
          writeFileSync(file, buffer)
          proxy.image.push({ product_id, url: imageUrl, filename })
        }
      })()
      console.log({
        pageNum,
        progress: `${i}/${links.length}`,
      })
    }
    if (pageNum >= maxPage) {
      console.log('reached last page:', {
        category_id,
        pageNum,
      })
      break
    }
    let count = links.length
    console.log({ pageNum, count })
    if (count === 0) {
      break
    }
  }
}
main()
