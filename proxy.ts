
import { proxySchema } from 'better-sqlite3-proxy'
import { db } from './db'

export type Site = {
  id?: number
  domain: string
}

export type Product = {
  id?: number
  title: string
  site_id: number
  url: string
}

export type Image = {
  id?: number
  product_id: number
  url: string
  filename: string
}

export type DBProxy = {
  site: Site[],
  product: Product[],
  image: Image[],
}

export let proxy = proxySchema<DBProxy>(db, {
  site: [],
  product: [],
  image: [],
})

