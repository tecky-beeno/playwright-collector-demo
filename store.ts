import { find } from 'better-sqlite3-proxy'
import { proxy } from './proxy'

export function getSiteId(domain: string): number {
  let row = find(proxy.site, { domain })
  if (row) {
    return row.id!
  }
  return proxy.site.push({ domain })
}
