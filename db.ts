import { toSafeMode, newDB } from 'better-sqlite3-schema'

export let dbFile = 'dev.sqlite3'

export let db = newDB({
  path: dbFile,
  migrate: false,
})

toSafeMode(db)
