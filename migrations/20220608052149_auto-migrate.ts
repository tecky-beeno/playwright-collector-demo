import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('site'))) {
    await knex.schema.createTable('site', table => {
      table.increments('id')
      table.text('domain').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('product'))) {
    await knex.schema.createTable('product', table => {
      table.increments('id')
      table.text('title').notNullable()
      table.integer('site_id').unsigned().notNullable().references('site.id')
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('image'))) {
    await knex.schema.createTable('image', table => {
      table.increments('id')
      table.integer('product_id').unsigned().notNullable().references('product.id')
      table.text('url').notNullable()
      table.text('filename').notNullable()
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('image')
  await knex.schema.dropTableIfExists('product')
  await knex.schema.dropTableIfExists('site')
}
